
import math


def bl_box (x) :
    return abs(math.sin(x))


lower=0
upper= 3.14159 
cumulative_area=0

N=100000


dx=(upper-lower)/N

for i in range(0,N):
    x1_2=lower+dx*(i+0.5)
    area=dx * (bl_box(x1_2))
    cumulative_area+=area


print(cumulative_area)
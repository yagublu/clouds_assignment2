# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    
    
    
    containerName="mapreducecontainer"
    
    #Get Input
    getInput= yield context.call_activity('GetInputDataFn', containerName)
    

    #Mapper
    mappers=[]
    for line in getInput:
        mappers.append(context.call_activity("Mapper",line))
    mappers_result= yield context.task_all(mappers)    
    mappers_flat = [item for sublist in mappers_result for item in sublist]
    


    #Shuffle
    shuffler = yield context.call_activity('Shuffler', mappers_flat)
    
    #Reduce
    reducers=[]
    for word in shuffler:
        reducers.append(context.call_activity("Reducer",tuple(word)))
    
    reducers_result = yield context.task_all(reducers)
    

    return reducers_result

main = df.Orchestrator.create(orchestrator_function)
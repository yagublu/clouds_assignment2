# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(input: tuple[str,list] )-> tuple:
    
    word=input[0]
    ls=input[1]
    num=0
    for l in ls:
        num+=1

    output=(word,num)    

    return output

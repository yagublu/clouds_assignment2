# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(input: list) -> list:
   
    word_dict={}

    for word_1_pair in input:
        word=word_1_pair[0]
        
        if word_dict.get(word) == None:
            word_dict[word]=[1]
        else:
            word_dict.get(word).append(1)


    output = [(key, value) for key, value in word_dict.items()]

   
    return output

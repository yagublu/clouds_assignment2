# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient


def main(containerName: str) -> list:
    
    
    
 
    # Quickstart code goes here
    blob_service_client = BlobServiceClient.from_connection_string("DefaultEndpointsProtocol=https;AccountName=mapreducestoragejey;AccountKey=mFUd8gFjLxYRQ4EqCDajVRjc0w+Zf2UqAUUFxjrcvHuB1HZnE1KA4dLUg1+BorkubXGVPjNc8M8G+ASt1AGaYA==;EndpointSuffix=core.windows.net")
 

    # List the blobs in the container
    container_client = blob_service_client.get_container_client(container=containerName) 
    blob_list = container_client.list_blobs()
    output = []
    for blob in blob_list:
        text = container_client.download_blob(blob.name).readall()
        lines = text.splitlines()
        lines_with_number = []
        i = 0
        for line in lines:
            i = i + 1
            lines_with_number.append((i, line.decode("utf-8")))
        output.extend(lines_with_number)
    return output
    


    


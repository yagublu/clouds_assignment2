from flask import Flask
from flask import request
import math


def bl_box (x) :
    return abs(math.sin(x))

lower=0
upper= 3.14159 




app = Flask(__name__)

@app.route('/')
def index():

        lower = request.args.get('lower', default = 0, type = float)
        upper = request.args.get('upper', default = 3.14159 ,type = float)
        print("lower: "+ str(lower)+"\nupper: "+str(upper))
        cumulative_areas=[]  
        N_s=[1,10,100,1000,10000,100000,1000000]    
        for N in N_s:    
            cumulative_area=0
            dx=(upper-lower)/N

            for i in range(0,N):
                x1_2=lower+dx*(i+0.5)
                area=dx * (bl_box(x1_2))
                cumulative_area+=area

            cumulative_areas.append(str(cumulative_area))
            
        output = "\n".join(cumulative_areas)
        print(output)
        return str(output)

app.run(host='0.0.0.0', port=81)
